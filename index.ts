const express = require("express");
const fs = require("fs");
const path = require("path");
const uuid = require("uuid");
const bbp = require("body-parser");
const bpc = require("blocking-proxy");
const es6promise = require("es6-promise");
const rwlock = require("rwlock");
const createDOMPurify = require("dompurify");
const { JSDOM } = require("jsdom");
const window1 = (new JSDOM("")).window;
const DOMPurify = createDOMPurify(window1);

// ---------- Consts
// ---------- File System paths
const webPath = "/dist/trexrunner";
const loginPath = "/login";
const spritesPath = "/assets/sprites";
const spritesPathAbsolute: string = path.join(__dirname, webPath, spritesPath);

// ---------- Api paths
const getCheckValidityPath = "/api/validity"; // 200ok if token is valid, 401 if invalid.
const getHighScoresPath = "/api/score/global/:numElems";
const getStatsPath = "/api/score/user";
const getSpritePath = "/api/sprite/:spriteId";

const postLoginPath = "/api/login";
const postRegisterPath = "/api/register";
const postResultPath = "/api/score"; // post with token and score
const postCommentPath = "/api/score/comment";

// Whether the checked registration credentials were invalid, already taken or valid.
enum newCredentialCheckResult {
    Invalid = 0,
    NameTaken = 1,
    Valid = 2,
}
// ---------- Data storage
// Users:    name(id)       password
const users: { [key: string]: string; } = {};

// tokens:    token         name(id)
const tokens: { [key: string]: string; } = {};

// scores for each user, scores ordered from oldest to newest
//                name(id)       scoreId[]
const userScores: { [key: string]: string[]; } = {};

// global scores: all score of all users ordered from lowest to highest
//                   scoreId         comment  score  timestamp name(id)
const globalScores: { [key: string]: [string, number, number, string]; } = {};

// ---------- Server
const app = express();

// for angular to render the page, nothing more like pictures and other resources
app.get(["/", "/*.js", "/*.ico", "/*.css", "/*.js.map"], express.static(path.join(__dirname, webPath)));

app.use(bbp.urlencoded({ extended: false }));
app.use(bbp.json());

// login
app.post(postLoginPath, logRequest, postSigninEPHandler);
app.post(postRegisterPath, logRequest, postRegisterEPHandler);

// serve for authenticated use
app.get(getCheckValidityPath, logRequest, getValidityEPHandler);

// Validate every request from now on.
app.use(checkToken);

// Share every other static resources only with login.
app.use("/", express.static(path.join(__dirname, webPath)));

// Log every request from now on.
app.use(logRequest);

app.get(getHighScoresPath, getHighScoresEPHandler2);
app.get(getStatsPath, getOwnScoresEPHandler);
app.get(getSpritePath, getSpriteEPHandler);

app.delete(postLoginPath, deleteLoginEPHandler);

app.post(postResultPath, postResultEPHandler);

function getSpriteEPHandler(req, res) {
    let fileName: string;

    if (!req.params || !req.params.spriteId || typeof req.params.spriteId !== "string") {
        return res.status(404).json({ reason: "No sprite with this id!", meme: "https://i.imgflip.com/1y3wkb.jpg" });
    } else {
        fileName = (String(req.params.spriteId) + ".png");
    }

    const fullPath = path.join(__dirname, "dist", "trexrunner", "assets", "sprites", encodeURIComponent(fileName));

    if (fs.existsSync(fullPath)) {
        return res.sendFile(fullPath);
    } else {
        return res.status(404).json({ reason: "No sprite with this id!", meme: "https://i.imgflip.com/1y3wkb.jpg" });
    }
}

function deleteLoginEPHandler(req, res) {
    const reqHeaderToken = req.header("Authorization");

    const user = tokens[reqHeaderToken];

    if (user === undefined) {
        return res.status(404).json(
            {
                meme: "https://i.imgflip.com/1y3wkb.jpg",
                reason: "There is no logged in user with your token!",
            });
    }

    delete tokens[reqHeaderToken];

    return res.status(200).json({ message: "You were logged out successfully!" });
}

function postResultEPHandler(req, res) {
    const reqHeaderToken = req.header("Authorization");

    if (req.body) {
        const score: number = parseFloat(req.body.score);
        const comment: string = DOMPurify.sanitize(req.body.comment).substring(0, 255);
        if (!isNaN(score) && score >= 0) {
            const uname = tokens[reqHeaderToken];
            const scoreId = uuid() as string;

            globalScores[scoreId] = [comment, score, Date.now(), uname];

            userScores[uname].push(scoreId);

            return res.status(200).json({ message: "Score saved!" });

        } else {
            return res.status(400).json({ reason: "Invalid score!" });
        }
    } else {
        return res.status(400).json({ reason: "Malformed request!" });
    }
}

function postCommentEPHandler(req, res) {
    const reqHeaderToken = req.header("Authorization");

    if (req.body) {
        const comment: string = DOMPurify.sanitize(req.body.comment);
        const scoreId: string = req.body.id;

        if (comment && scoreId && globalScores[scoreId]) {

            //                            score belongs to user                    score is empty
            const canEdit = globalScores[scoreId][3] === tokens[reqHeaderToken] && !globalScores[scoreId][0];

            if (canEdit) {

                globalScores[scoreId][0] = comment;

                return res.status(200).json({ message: "Comment saved!" });
            } else {
                return res.status(400).json({ reason: "You cannot edit that score!" });
            }
        }
    } else {
        return res.status(400).json({ reason: "Malformed request!" });
    }
}

function getHighScoresEPHandler2(req, res) {
    const uname = tokens[req.header("Authorization")];

    if (!req.params || !req.params.numElems) {
        return res.status(400).json({ reason: "Malformed request!" });
    } else {
        const num: number = parseInt(req.params.numElems, 10);

        const orderedUserHighScores = Object.keys(userScores).map((key) => userScores[key])
            .filter((el) => el && el.length > 0)
            .map((userScoreArr) => {
                return userScoreArr.reduce((acc, x) => {
                    const xScore = globalScores[x][1];

                    if (xScore > acc[1]) {
                        return globalScores[x];
                    } else {
                        return acc;
                    }
                },
                    [null, -1, null, null]);
            })
            .filter((el) => el && el[3])
            .sort((a, b) => {
                // If two scores are the equal, then the older score is "higher".
                if (b[1] === a[1]) {
                    return b[2] - a[2];
                } else {
                    return b[1] - a[1];
                }
            });

        const scoreLength = orderedUserHighScores.length;

        const highScores = orderedUserHighScores
            .slice(0, num)
            .map((el) => {
                return { name: el[3], score: el[1] };
            });

        const userScore = userScores[uname];

        if (userScore === undefined || userScore.length === 0) {
            return res.status(200).json({ ranking: -1, total: scoreLength, userScore: -1, scores: highScores });
        } else {
            const ind = orderedUserHighScores.findIndex((x) => x[3] === uname);

            if (ind === -1) {
                return res.status(500).json({ reason: "Internal server error!" });
            }

            const userHighScore: number = orderedUserHighScores[ind][1];
            return res.status(200).json({
                ranking: ind + 1,
                scores: highScores,
                total: scoreLength,
                userScore: userHighScore,
            });
        }
    }
}

function getHighScoresEPHandler(req, res) {
    const uname = tokens[req.header("Authorization")];

    if (!req.params || !req.params.numElems) {
        return res.status(400).json({ reason: "Malformed request!" });
    } else {
        const num: number = parseInt(req.params.numElems, 10);

        const orderedScores = Object.keys(globalScores)
            .map((key) => globalScores[key])
            .sort((a, b) => {
                // If two scores are the equal, then the older score is "higher".
                if (b[1] === a[1]) {
                    return b[2] - a[2];
                } else {
                    return b[1] - a[1];
                }
            });

        const scoreLength = orderedScores.length;
        const highScores = orderedScores
            .slice(0, num)
            .map((el) => {
                return { name: el[3], score: el[1] };
            });

        const userScore = userScores[uname];

        if (userScore === undefined || userScore.length === 0) {
            return res.status(200).json({ ranking: -1, total: scoreLength, userScore: -1, scores: highScores });
        } else {
            const ind = orderedScores.findIndex((x) => x[3] === uname);

            if (ind === -1) {
                return res.status(500).json({ reason: "Internal server error!" });
            }

            const userHighScore: number = orderedScores[ind][1];
            return res.status(200).json({
                ranking: ind + 1,
                scores: highScores,
                total: scoreLength,
                userScore: userHighScore,
            });
        }

    }
}

function getOwnScoresEPHandler(req, res) {
    const reqHeaderToken = req.header("Authorization");
    const userScore = userScores[tokens[reqHeaderToken]];

    if (userScore === undefined) {
        return res.status(200).json({ scores: [] });
    }

    const result = userScore
        .map((key) => {
            const el = globalScores[key];

            return { score: el[1], date: el[2], comment: el[0] };
        });

    return res.status(200).json({ scores: result });
}

function getValidityEPHandler(req, res) {
    const reqHeaderToken = req.header("Authorization");

    if (reqHeaderToken == null || reqHeaderToken === undefined) {
        res.status(401).json({ reason: "Please log in!" });
        return;
    }

    if (tokens[reqHeaderToken]) {
        res.status(200).json({ message: "success" });
    } else {
        res.status(401).json({ reason: "Please log in!" });
    }
}

function checkToken(req, res, next) {
    const reqHeaderToken = req.header("Authorization");

    if (reqHeaderToken == null || reqHeaderToken === undefined) {
        res.status(401).json({ reason: "Please log in!" });
        return;
    }

    if (tokens[reqHeaderToken]) {
        next();
    } else {
        res.status(401).json({ reason: "Please log in!" });
    }
}

function postSigninEPHandler(req, res) {
    if (req.body) {
        const uname = req.body.username;
        const passwd = req.body.password;

        if (validateLoginData(uname, passwd)) {
            const user = users[uname];

            if (user) {
                const newToken = uuid() as string;
                tokens[newToken] = uname;

                return res.status(200).json({ token: newToken });
            }
        } else {
            return res.status(401).json({ reason: "Invalid username or password!" });
        }
    } else {
        return res.status(400).json({ reason: "Malformed request!" });
    }
}

function postRegisterEPHandler(req, res) {
    if (req.body) {
        const uname = req.body.username;
        const passwd = req.body.password;

        if (uname == null || passwd == null
            || uname === undefined || passwd === undefined
            || uname === "undefined" || passwd === "undefined") {
            return res.status(401).json({ reason: "Invalid username or password!" });
        }

        switch (allowedCredentials(uname, passwd)) {
            case 0:
                return res.status(401).json({ reason: "Invalid username or password!" });
            case 1:
                return res.status(406).json({ reason: "Username already taken!" });
            case 2:
                // console.log("user passed check with name: " + uname + " and pw: " + passwd);
                break;
            default:
                // console.log("invalid state in reg check");
                return res.status(500).json({ reason: "Internal server error!" });
        }

        users[uname] = passwd;
        const newToken = uuid() as string;
        tokens[newToken] = uname;
        userScores[uname] = [];
        return res.status(200).json({ token: newToken });
    } else {
        return res.status(400).json({ reason: "Malformed request!" });
    }
}

function validateLoginData(uname: string, passwd: string): boolean {
    const purifiedName: string = DOMPurify.sanitize(uname.trim());
    const purifiedPassword: string = DOMPurify.sanitize(passwd.trim());

    if (!purifiedName || !purifiedPassword
        || uname !== purifiedName || passwd !== purifiedPassword
        || purifiedName.trim().length === 0 || purifiedPassword.trim().length === 0) {
        return false;
    }

    const storedPassword = users[uname];
    return storedPassword === purifiedPassword;
}

function allowedCredentials(uname: string, passwd: string): newCredentialCheckResult {
    if (uname !== undefined && passwd !== undefined && uname.trim().length > 0 && passwd.trim().length > 0) {
        const purifiedName: string = DOMPurify.sanitize(uname.trim());
        const purifiedPassword: string = DOMPurify.sanitize(passwd.trim());

        if (purifiedName === uname && purifiedPassword === passwd
            && purifiedName.trim().length > 0 && purifiedPassword.trim().length > 0) {

            if (!users[purifiedName]) {
                return newCredentialCheckResult.Valid;
            } else {
                return newCredentialCheckResult.NameTaken;
            }
        }
    }

    return newCredentialCheckResult.Invalid;
}

function logRequest(req, res, next) {
    const date = new Date();
    console.log(`${date.toISOString()} | ${req.ip} -> ${req.method}: ${req.url}`);
    next();
}

function mod(x: number, y: number): number {
    return ((x % y) + y) % y;
}

app.listen(3001);

console.log("************************************\nServer is running at 127.0.0.1:3001\n************************************\n");