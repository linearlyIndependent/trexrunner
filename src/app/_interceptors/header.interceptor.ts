import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { DataUserService } from '../_services/data-user.service';

@Injectable()
export class HttpHeaderInterceptor implements HttpInterceptor {
  constructor(private userData: DataUserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const changedReq = req.clone({
      headers: req.headers.set('Authorization', this.userData.userToken)
    });

    return next.handle(changedReq);
  }
}
