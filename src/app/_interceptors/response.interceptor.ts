import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


import { DataUserService } from '../_services/data-user.service';

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {
    constructor(private userData: DataUserService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                return event;
            }),
            catchError((error: HttpErrorResponse) => {

                if (error.status === 401) {
                    this.userData.deleteUserData();
                    this.router.navigate(['/login']);
                }

                let data = {};
                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };

                return throwError(error);
            }));
    }
}
