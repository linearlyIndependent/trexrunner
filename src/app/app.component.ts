import { Component } from '@angular/core';

import { DataUserService } from './_services/data-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private _title = 'trexrunner';

  public get title(): string {
    return this._title;
  }

  public get isLoggedIn(): boolean {
    return this.userDataService.isLoggedIn;
  }

  public get isLoggedOut(): boolean {
    return !this.isLoggedIn;
  }

  public get userName(): string {
    return this.userDataService.userName;
  }


  constructor(private userDataService: DataUserService, private router: Router) {
    this.checkToken();
  }

  private checkToken() {
    this.userDataService.checkTokenValidity()
      .subscribe(data => {
        if (data) {
          this.userDataService.isLoggedIn = true;
        } else {
        }
      }, err => {
      });
  }
}
