import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { HttpHeaderInterceptor } from './_interceptors/header.interceptor';
import { HttpResponseInterceptor } from './_interceptors/response.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { StatsGlobalComponent } from './stats-global/stats-global.component';
import { StatsUserComponent } from './stats-user/stats-user.component';
import { LoginComponent } from './login/login.component';
import { GameComponent } from './game/game.component';
import { HashLocationStrategy } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    StatsGlobalComponent,
    StatsUserComponent,
    LoginComponent,
    GameComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    // HashLocationStrategy RouterModule.for
    // BrowserAnimationsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpHeaderInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpResponseInterceptor,
    multi: true
  }],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
