import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { DataUserService } from './../_services/data-user.service';
import { RoutingHistoryService } from '../_services/routing-history.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private userDataService: DataUserService, private router: Router, private routerHistory: RoutingHistoryService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin(state.url);
  }

  checkLogin(url: string): boolean {
    // If logged in, login page should redirect to the game.
    if (url === '/login') {
      if (this.userDataService.isLoggedIn) {

        if (this.routerHistory.previous !== '') {
          this.router.navigate([this.routerHistory.previous]);
        } else {
          this.router.navigate(['/game']);
        }

        return false;
      } else {
        return true;
      }
    }


    // If not logged in, every page should redirect to the login page.
    if (this.userDataService.isLoggedIn) {

      if (url !== '/logout') {
        this.routerHistory.previous = url;
      }

      return true;
    } else {
      this.routerHistory.previous = url;
      this.router.navigate(['/login']);
      return false;
    }
  }
}
