import { Component, OnInit, HostListener } from '@angular/core';
import { GameService } from '../_services/game.service';
import { GameNetworkService } from '../_services/game-network.service';
import { GameElement } from '../_container-classes/game-element';
import { GameElementTrex } from '../_container-classes/game-element-trex';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  comment: string;
  alert: string;
  _freeHeight: number;
  _freeWidth: number;
  invalidSize: boolean;

  public get gameEnded(): boolean {
    return this.gameService.gameEnded;
  }

  public get imageLoaded(): boolean {
    return this.gameService.imageLoaded;
  }

  public get isRunning(): boolean {
    return this.gameService.isRunning;
  }

  public get ground(): { xPos: number; yPos: number; spriteSrc: string | ArrayBuffer; } {
    return this.gameService.ground;
  }

  public get trex(): GameElementTrex {
    return this.gameService.trex;
  }

  public get enemies(): GameElement[] {
    return this.gameService.enemies;
  }

  public get boardSize(): { height: number; width: number; scaling: number; } {
    return this.gameService.boardSize;
  }

  public get score(): number {
    return Math.floor(this.gameService.score * 1000);
  }

  public get isGod(): boolean {
    return this.gameService.trex.isGod;
  }

  public get godUsed(): boolean {
    return this.gameService.trex.godUsed;
  }


  constructor(
    private gameService: GameService,
    private gameNetwork: GameNetworkService,
    private router: Router) {

  }

  ngOnInit() {
    this.gameService.reset();
    this.gameService.imageLoaded = false;
    this.comment = '';
    this.alert = '';
    this.getSprite();
  }

  getSprite() {
    this.gameNetwork.getSprite('trex1')
      .subscribe(blob => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.addEventListener('load', () => {
          this.gameService.spriteSrc = reader.result;
          this.gameService.imageLoaded = true;
        }, false);

        if (blob) {
          reader.readAsDataURL(blob);
        }

      }, err => {
      });


    this.gameNetwork.getSprite('trex2')
      .subscribe(blob => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.addEventListener('load', () => {
          this.gameService.spriteAltSrc = reader.result;

        }, false);

        if (blob) {
          reader.readAsDataURL(blob);
        }

      }, err => {
        console.log('Getting sprite failed!');
      });
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
    switch (event.key) {
      case ' ':
      case 'ArrowUp':
        this.gameService.keyState.jumpPressed = false;
        break;
      case 'Control':
      case 'ArrowDown':
        this.gameService.keyState.crouchPressed = false;
        break;
      case 'M':
      case 'm':
        this.gameService.keyState.extraPressed = false;
        break;
      case 'Escape':
        this.gameService.keyState.escapePressed = false;
        break;
      default:
        break;
    }
  }

  @HostListener('document:keydown', ['$event'])
  onKey(event: KeyboardEvent) {
    switch (event.key) {
      case ' ':
      case 'ArrowUp':
        if (!this.gameService.keyState.jumpPressed) {
          this.gameService.keyState.jumpPressed = true;
        }
        break;
      case 'Control':
      case 'ArrowDown':
        if (!this.gameService.keyState.crouchPressed) {
          this.gameService.keyState.crouchPressed = true;
        }
        break;
      case 'M':
      case 'm':
        if (!this.gameService.keyState.extraPressed) {
          this.gameService.keyState.extraPressed = true;
        }
        break;
      case 'Escape':
        if (!this.gameService.keyState.escapePressed) {
          this.gameService.keyState.escapePressed = true;
        }

        break;
      default:
        break;
    }
  }

  startClickedCallback(): void {
    this.gameService.start();
  }


  sendScore(points: number, commentary: string): void {
    if (!commentary) {
      commentary = '';
    }

    this.gameNetwork.postScore(points, commentary)
      .subscribe(data => {
        window.location.reload();
      }, err => {
        window.location.reload();
      });
  }

  yesClickedCallback(): void {
    this.sendScore(this.score, this.comment);
  }

  noClickedCallback(): void {
    this.sendScore(this.score, '');
  }
}
