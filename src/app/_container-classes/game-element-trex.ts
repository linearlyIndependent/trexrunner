import { GameElement } from './game-element';

export class GameElementTrex {
    private _gameElement: GameElement;
    private _floatTime: number;
    private _jumpRange: { min: number; max: number; };
    private _spriteAltSrc: string | ArrayBuffer;
    private _state: GameElementTrex.State;
    private _isGod: boolean;
    private _elapsedFloat: number;
    private _jumpState: GameElementTrex.JumpState;
    private _crouchTime: number;
    private _godTime: number;
    private _elapsedGod: number;
    private _godAlreadyUsed: boolean;
    private _crouchTimeElapsed: number;

    public get gameElement(): GameElement {
        return this._gameElement;
    }

    public get pos(): { xPos: number; yPos: number; } {
        return this._gameElement.pos;
    }

    public get left(): number {
        return this._gameElement.left;
    }

    public get right(): number {
        return this._gameElement.right;
    }

    public get bottom(): number {
        return this._gameElement.bottom;
    }

    public get top(): number {
        return this._gameElement.top;
    }

    public get spriteSrc(): string | ArrayBuffer {
        return this._isGod && this._spriteAltSrc ? this._spriteAltSrc : this._gameElement.spriteSrc;
    }

    public get type(): GameElement.Type {
        return this._gameElement.type;
    }

    public get typeName(): string {
        return this._gameElement.typeName + `-${this.state}`;
    }

    public get state(): GameElementTrex.State {
        return this._state;
    }

    public get godUsed(): boolean {
        return this._godAlreadyUsed;
    }


    public set godUsed(v: boolean) {
        this._godAlreadyUsed = v;
    }


    public set state(v: GameElementTrex.State) {
        this._state = v;
    }

    public get isGod(): boolean {
        return this._isGod;
    }

    public set isGod(v: boolean) {
        this._isGod = v;
    }

    constructor(
        pos: { xPos: number; yPos: number; },
        spriteSrc: string | ArrayBuffer,
        spriteAltSrc: string | ArrayBuffer, // alternative sprite for the god mode
        state: GameElementTrex.State = GameElementTrex.State.Init,
        jumpRange: { min: number; max: number },     // height
        crouchTime: number = 0.2,
        floatTime: number = 1.3, // time which the trex floats in a jump before falling down
        isGod: boolean = false,
        godTime: number = 6) {

        this._gameElement = new GameElement(
            GameElement.Type.TRex,
            pos,
            spriteSrc,
            0,
            1,
            GameElement.Type.TRex + '-' + GameElementTrex.State.Walk);

        this._spriteAltSrc = spriteAltSrc;
        this._jumpRange = jumpRange;
        this._state = state;
        this._floatTime = floatTime;
        this._elapsedFloat = 0;
        this._jumpState = GameElementTrex.JumpState.Up;
        this._isGod = isGod;
        this._godTime = godTime;
        this._elapsedGod = 0;
        this._godAlreadyUsed = false;
        this._crouchTime = crouchTime;
        this._crouchTimeElapsed = 0;
    }

    // Switches the Trex from init mode to walking.
    startWalking(): void {
        if (this._state === GameElementTrex.State.Init) {
            this._state = GameElementTrex.State.Walk;
        }
    }

    // Trex's dedicated update method.
    update(crouch: boolean, jump: boolean, extra: boolean, dt: number, speedChange: number): void {

        // What's dead should stay dead...
        if (this._state === GameElementTrex.State.Dead) {
            return;
        }

        // If both pressed then jump is invalid.
        if (crouch && jump) {
            jump = false;
        }

        // Handle ongoing god mode.
        if (this._isGod) {
            this._elapsedGod += dt;

            if (this._elapsedGod > this._godTime) {
                this._isGod = false;
                this._godAlreadyUsed = true;
            }
        } else if (extra && !this._godAlreadyUsed) {
            this._isGod = true;
            this._elapsedGod = 0;
        }

        switch (this._state) {
            case GameElementTrex.State.Init:
                this._state = GameElementTrex.State.Walk;
                break;
            case GameElementTrex.State.Walk:
                if (crouch) {
                    this._state = GameElementTrex.State.Bent;
                    this._crouchTimeElapsed = 0;
                    this.gameElement.size = GameElement.GameElementSize[this.typeName];
                } else if (jump) {
                    this._state = GameElementTrex.State.Jump;
                    this._elapsedFloat = 0;
                    this._jumpState = GameElementTrex.JumpState.Up;
                    this._gameElement.speed = GameElement.GameElementSpeed[this.typeName];
                }

                break;
            case GameElementTrex.State.Jump:

                if (crouch && this._jumpState !== GameElementTrex.JumpState.Down) {
                    this._jumpState = GameElementTrex.JumpState.Down;

                    this._gameElement.speed = {
                        vertical: -GameElement.GameElementSpeed[this.typeName].vertical * 10,
                        horizontal: 0
                    };
                }

                switch (this._jumpState) {
                    case GameElementTrex.JumpState.Up:

                        if (this.top + this._gameElement.speed.vertical > this._jumpRange.max) {

                            this._jumpState = GameElementTrex.JumpState.Float;
                            this._gameElement.speed = { vertical: 0, horizontal: 0 };
                        }

                        break;
                    case GameElementTrex.JumpState.Float:
                        if (!this.isGod) {
                            this._elapsedFloat += dt;
                        }

                        if (this._elapsedFloat > this._floatTime && !this.isGod) {
                            this._jumpState = GameElementTrex.JumpState.Down;

                            this._gameElement.speed = {
                                vertical: -GameElement.GameElementSpeed[this.typeName].vertical,
                                horizontal: 0
                            };
                        }

                        break;
                    case GameElementTrex.JumpState.Down:

                        if (this.bottom + this._gameElement.speed.vertical < this._jumpRange.min) {
                            this._state = GameElementTrex.State.Walk;
                            this._gameElement.pos.yPos = this._jumpRange.min;
                            this._gameElement.speed = GameElement.GameElementSpeed[this.typeName];
                        }

                        break;

                    default:
                        console.log('invalid trex jumpstate');
                        break;
                }

                break;
            case GameElementTrex.State.Bent:

                if (crouch) {
                    this._crouchTimeElapsed = 0;
                } else {
                    this._state = GameElementTrex.State.Walk;
                    this.gameElement.size = GameElement.GameElementSize[this.typeName];
                }

                break;
            default:
                console.log('invalid state in update trex');
                break;
        }

        this._gameElement.update(speedChange);
    }
}

export namespace GameElementTrex {

    export enum State {
        Init = 'init',
        Walk = 'walk',
        Jump = 'jump',
        Bent = 'bent',
        Dead = 'dead',
    }

    export enum JumpState {
        Up = 'up',
        Float = 'float',
        Down = 'down',
    }
}
