import { GameElementTrex } from './../_container-classes/game-element-trex';

export class GameElement {
    type: GameElement.Type;
    subtype: number;
    size: { height: number; width: number; };
    pos: { xPos: number; yPos: number; };
    private _speed: { vertical: number; horizontal: number; };
    spriteSrc: string | ArrayBuffer;
    state: GameElementTrex.State;
    speedMultiplicator: number;

    public get left(): number {
        return this.pos.xPos;
    }

    public get right(): number {
        return this.pos.xPos + this.size.width;
    }

    public get bottom(): number {
        return this.pos.yPos;
    }

    public get top(): number {
        return this.pos.yPos + this.size.height;
    }

    public get typeName(): string {
        switch (this.type) {
            case GameElement.Type.TRex:
            case GameElement.Type.Bird:
                return this.type;
            case GameElement.Type.Cactus:
            case GameElement.Type.CactusLarge:
                return this.type + `-${this.subtype}`;
            default:
                return this.type;
        }
    }

    public get speed(): { vertical: number; horizontal: number; } {
        return {vertical: this._speed.vertical * this.speedMultiplicator, horizontal: this._speed.horizontal * this.speedMultiplicator};
    }


    public set speed(v: { vertical: number; horizontal: number; }) {
        this._speed = v;
    }

    constructor(
        type: GameElement.Type,
        pos: { xPos: number; yPos: number; },
        spriteUrl: string | ArrayBuffer,
        subtype: number = 0,
        speedMultipl: number = 1,
        trexArg: string = '') {

        this.type = type;
        this.subtype = subtype;
        this.pos = pos;

        // Trex has special subtypes, handled by the Trex's class itself.
        if (type === GameElement.Type.TRex) {
            this.size = GameElement.GameElementSize[trexArg];
            this._speed = GameElement.GameElementSpeed[trexArg];
        } else {
            this.size = GameElement.GameElementSize[this.typeName];
            this._speed = GameElement.GameElementSpeed[this.typeName];
        }

        this.speedMultiplicator = speedMultipl;

        this.spriteSrc = spriteUrl;
    }

    update(speedChange: number) {
        this.pos.xPos += (this.speed.horizontal * speedChange);
        this.pos.yPos += (this.speed.vertical * speedChange);
    }
}

export namespace GameElement {
    export enum Type {
        TRex = 'trex',
        Bird = 'bird',
        Cactus = 'cactus',
        CactusLarge = 'cactus-large',
    }

    export const EnemySubtypes: { [key: string]: { numberOfSubtypes: number }; } = {};
    EnemySubtypes['bird'] = { numberOfSubtypes: 0 };
    EnemySubtypes['cactus'] = { numberOfSubtypes: 3 };
    EnemySubtypes['cactus-large'] = { numberOfSubtypes: 3 };

    // size of bounding box          subtype
    export const GameElementSize: { [key: string]: { height: number, width: number }; } = {};
    GameElementSize['trex-walk'] = { height: 50, width: 45 };
    GameElementSize['trex-init'] = { height: 50, width: 45 };
    GameElementSize['trex-jump'] = { height: 50, width: 45 };
    GameElementSize['trex-dead'] = { height: 50, width: 45 };
    GameElementSize['trex-bent'] = { height: 30, width: 58 };
    GameElementSize['bird'] = { height: 50, width: 45 };
    GameElementSize['cactus-0'] = { height: 40, width: 17 };
    GameElementSize['cactus-1'] = { height: 40, width: 34 };
    GameElementSize['cactus-2'] = { height: 40, width: 50 };
    GameElementSize['cactus-large-0'] = { height: 55, width: 25 };
    GameElementSize['cactus-large-1'] = { height: 55, width: 50 };
    GameElementSize['cactus-large-2'] = { height: 55, width: 74 };


    export const GameElementSpeed: { [key: string]: { vertical: number, horizontal: number }; } = {};
    GameElementSpeed['trex-walk'] = { vertical: 0, horizontal: 0 };
    GameElementSpeed['trex-init'] = { vertical: 0, horizontal: 0 };
    GameElementSpeed['trex-jump'] = { vertical: 200 / 60, horizontal: 0 };
    GameElementSpeed['trex-dead'] = { vertical: 0, horizontal: 0 };
    GameElementSpeed['trex-bent'] = { vertical: 0, horizontal: 0 };
    GameElementSpeed['bird'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-0'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-1'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-2'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-large-0'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-large-1'] = { vertical: 0, horizontal: -80 / 60 };
    GameElementSpeed['cactus-large-2'] = { vertical: 0, horizontal: -80 / 60 };
}
