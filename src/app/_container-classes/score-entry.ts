export class ScoreEntry {
    private _comment: string;
    private _score: number;
    private _date: number;
    private _dateString: string;

    public get comment(): string {
        return this._comment;
    }

    public get score(): number {
        return this._score;
    }

    public get dateString(): string {
        return this._dateString;
    }

    constructor(comment: string, score: number, date: number = 0) {
        this._comment = comment;
        this._score = score;
        this._date = date;
        this._dateString = (new Date(date)).toDateString();
    }
}
