export class GameField {
    height: number; // px
    width: number; // px
    scalingFactor: number; // between 1-3

    constructor(height: number = 450, width: number = 1200, scaling: number = 1) {
        this.scalingFactor = scaling;
        this.height = height;
        this.width = width;
    }
}
