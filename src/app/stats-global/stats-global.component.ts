import { Component, OnInit } from '@angular/core';

import { StatsGlobalService } from '../_services/stats-global.service';
import { DataUserService } from '../_services/data-user.service';
import { ScoreEntry } from '../_container-classes/score-entry';

@Component({
  selector: 'app-stats-global',
  templateUrl: './stats-global.component.html',
  styleUrls: ['./stats-global.component.scss']
})
export class StatsGlobalComponent implements OnInit {
  private _scores: ScoreEntry[];
  private _ranking: number;
  private _total: number;
  private _userScore: number;
  private _validStats: boolean;
  private _validScore: boolean;
  _validRanking: boolean;

  public get scores(): ScoreEntry[] {
    return this._scores;
  }

  public get ranking(): number {
    return this._ranking;
  }

  public get total(): number {
    return this._total;
  }

  public get userScore(): number {
    return this._userScore;
  }

  public get validRanking(): boolean {
    return this._validRanking;
  }

  public get validStats(): boolean {
    return this._validStats;
  }

  constructor(private globalStatService: StatsGlobalService, private userDataService: DataUserService) {
  }

  ngOnInit() {
    this._scores = [];

    this._scores = [];
    this._ranking = -1;
    this._userScore = -1;
    this.getScores();
  }

  private getScores() {
    this.globalStatService.getScores()
      .subscribe(data => {

        if (data
          && (data['ranking'] || data['ranking'] === 0)
          && (data['total'] || data['total'] === 0)
          && (data['userScore'] || data['userScore'] === 0)
          && data['scores']) {
          this._ranking = data['ranking'];
          this._total = data['total'];
          this._userScore = data['userScore'];
          this._scores = (data['scores']).map(x => new ScoreEntry(x['name'], x['score']));

          this._validScore = this._userScore > 0;
          this._validRanking = this._ranking > 0;
          this._validStats = this._scores.length > 0;

        } else {
        }
      }, err => {
        // return callback(false);
      });
  }

}
