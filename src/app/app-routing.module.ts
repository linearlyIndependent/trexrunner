import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { GameComponent } from './game/game.component';
import { StatsGlobalComponent } from './stats-global/stats-global.component';
import { StatsUserComponent } from './stats-user/stats-user.component';
import { LoginGuard } from './_guards/login.guard';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [LoginGuard] },
  { path: '', redirectTo: 'game', pathMatch: 'full', canActivate: [LoginGuard] },
  { path: 'game', component: GameComponent, canActivate: [LoginGuard], runGuardsAndResolvers: 'always' },
  { path: 'ranking', component: StatsGlobalComponent, canActivate: [LoginGuard] },
  { path: 'stats', component: StatsUserComponent, canActivate: [LoginGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
