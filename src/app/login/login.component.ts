import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './../_services/login.service';
import { DataUserService } from './../_services/data-user.service';
import { RoutingHistoryService } from '../_services/routing-history.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  _title: string;
  _uname: string;
  _passwd: string;
  _alert: string;


  public get title(): string {
    return this._title;
  }

  public get uname(): string {
    return this._uname;
  }

  public set uname(v: string) {
    this._uname = v;
  }

  public get passwd(): string {
    return this._passwd;
  }

  public set passwd(v: string) {
    this._passwd = v;
  }

  public get alert(): string {
    return this._alert;
  }

  constructor(
    private loginService: LoginService,
    private userDataService: DataUserService,
    private router: Router,
    private routerHistory: RoutingHistoryService) {
  }

  ngOnInit() {
    this._title = 'Trex Runner';
    this._alert = 'Please log in or register to access the game!';
    this.checkToken();
  }

  private checkToken(): any {
    this.userDataService.checkTokenValidity()
      .subscribe(data => {
        if (data && this.userDataService.userToken) {
          this._alert = `Hi, ${this.userDataService.userName}!`;
          this.userDataService.isLoggedIn = true;

          if (this.routerHistory.previous !== '') {
            this.router.navigate([this.routerHistory.previous]);
          } else {
            this.router.navigate(['/game']);
          }

        } else {
        }
      }, err => {
      });
  }

  onLoginClickCallback(): void {
    this.loginService.loginUser(this.uname, this.passwd)
      .subscribe(data => {
        if (data && data['token']) {
          this.userDataService.userName = this._uname;
          this.userDataService.userToken = data['token'];
          this._alert = `Hi, ${this._uname}!`;

          if (this.routerHistory.previous !== '') {
            this.router.navigate([this.routerHistory.previous]);
          } else {
            this.router.navigate(['/game']);
          }
        } else {
          this._alert = 'Try again!';
        }

      }, err => {
        this._alert = 'Try again!';
      });
  }

  onRegisterClickCallback(): void {
    this.loginService.registerUser(this.uname, this.passwd)
      .subscribe(data => {
        if (data && data['token']) {
          this.userDataService.userName = this._uname;
          this.userDataService.userToken = data['token'];
          this._alert = `Welcome to the game ${this._uname}!`;

          if (this.routerHistory.previous !== '') {
            this.router.navigate([this.routerHistory.previous]);
          } else {
            this.router.navigate(['/game']);
          }
        } else {
          this._alert = 'Try again!';
        }

      }, err => {
        this._alert = 'Try again!';
      });
  }
}
