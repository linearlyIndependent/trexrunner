import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { DataConnectionService } from '../_services/data-connection.service';

@Injectable({
  providedIn: 'root'
})
export class StatsGlobalService {

  constructor(private httpClient: HttpClient, private connectionService: DataConnectionService) {
  }

  getScores(numScores: number = 10) {
    return this.httpClient.get(this.connectionService.getHighScoresPath + `/${numScores}`);
  }
}
