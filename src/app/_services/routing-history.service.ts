import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoutingHistoryService {
  previous: string;

  constructor() {
    this.previous = '';
  }
}
