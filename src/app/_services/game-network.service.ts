import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { DataConnectionService } from '../_services/data-connection.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameNetworkService {

  constructor(private httpClient: HttpClient, private connectionService: DataConnectionService) {

  }

  getSprite(id: string = 'trex1'): Observable<Blob> {
    return this.httpClient.get(this.connectionService.getSpritePath + `/${id}`, { responseType: 'blob' });
  }

  postScore(score: number, comment: string) {
    comment = comment.substring(0, 255);

    return this.httpClient.post(
      this.connectionService.postResultPath,
      'score=' + score + '&comment=' + comment,
      { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) });
  }
}
