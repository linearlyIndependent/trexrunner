import { TestBed } from '@angular/core/testing';

import { GameNetworkService } from './game-network.service';

describe('GameNetworkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameNetworkService = TestBed.get(GameNetworkService);
    expect(service).toBeTruthy();
  });
});
