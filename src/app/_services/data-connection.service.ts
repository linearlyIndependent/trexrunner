import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataConnectionService {
  private _getCheckValidityPath: string;
  private _getHighScoresPath: string;
  private _getStatsPath: string;

  private _postLoginPath: string;
  private _postRegisterPath: string;
  private _postResultPath: string;
  private _getSpritePath: string;
  private _postCommentPath: string;

  public get getCheckValidityPath(): string {
    return this._getCheckValidityPath;
  }

  public get getHighScoresPath(): string {
    return this._getHighScoresPath;
  }

  public get getStatsPath(): string {
    return this._getStatsPath;
  }

  public get postLoginPath(): string {
    return this._postLoginPath;
  }

  public get postRegisterPath(): string {
    return this._postRegisterPath;
  }

  public get postResultPath(): string {
    return this._postResultPath;
  }

  public get postCommentPath(): string {
    return this._postCommentPath;
  }

  public get getSpritePath(): string {
    return this._getSpritePath;
  }

  constructor() {
    this._getCheckValidityPath = '/api/validity';
    this._getHighScoresPath = '/api/score/global';
    this._getStatsPath = '/api/score/user';
    this._getSpritePath = '/api/sprite';
    this._postLoginPath = '/api/login';
    this._postRegisterPath = '/api/register';
    this._postResultPath = '/api/score';
    this._postCommentPath = '/api/score/comment';
  }
}
