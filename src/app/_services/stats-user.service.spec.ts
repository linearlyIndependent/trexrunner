import { TestBed } from '@angular/core/testing';

import { StatsUserService } from './stats-user.service';

describe('StatsUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatsUserService = TestBed.get(StatsUserService);
    expect(service).toBeTruthy();
  });
});
