import { TestBed } from '@angular/core/testing';

import { StatsGlobalService } from './stats-global.service';

describe('StatsGlobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatsGlobalService = TestBed.get(StatsGlobalService);
    expect(service).toBeTruthy();
  });
});
