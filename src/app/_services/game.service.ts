import { Injectable } from '@angular/core';

import { GameElement } from './../_container-classes/game-element';
import { GameElementTrex } from './../_container-classes/game-element-trex';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private _elapsed: number;
  private _now: number;
  private _dt: number;   // time elapsed between frames
  private _last: number; // timestamp
  private _step: number;
  private _slowStep: number;

  private _level: number;
  private _levelDuration: number;
  private _minimumDistanceEnemy: number;
  private _totalEnemiesLevel: number;
  private _enemiesGeneratedLevel: number;
  private _enemyTimeSegment: number;
  private _speedChangeLevel: number;
  private _levelElapsedTime: number;
  private _score: number;

  boardSize: { height: number; width: number; scaling: number; };
  ground: { typeName: string; xPos: number; yPos: number; spriteSrc: string | ArrayBuffer; };
  spriteAltSrc: string | ArrayBuffer;
  spriteSrc: string | ArrayBuffer;
  imageLoaded: boolean;
  isRunning: boolean;
  gameEnded: boolean;

  trex: GameElementTrex;
  enemies: GameElement[];

  keyState: { jumpPressed: boolean; crouchPressed: boolean; escapePressed: boolean; extraPressed: boolean; };
  resultNotifier: BehaviorSubject<{ points: number; forceQuit: boolean; }>;
  _speedChangeLevelBase: number;

  public get score(): number {
    return this._score;
  }


  constructor() {
    this.keyState = { jumpPressed: false, crouchPressed: false, escapePressed: false, extraPressed: false };
    this.isRunning = false;
    this.gameEnded = false;
    this.imageLoaded = false;
    this._now = 0;
    this._score = 0;
    this.boardSize = { height: 150, width: 400, scaling: 1 };

    this.frame = this.frame.bind(this); // Get bound to this.
  }

  private init() {
    this.keyState = { jumpPressed: false, crouchPressed: false, escapePressed: false, extraPressed: false };
    this.ground = { typeName: 'ground-0', xPos: 0, yPos: 15, spriteSrc: this.spriteSrc };
    this.trex = new GameElementTrex(
      { xPos: 10, yPos: 0 },
      this.spriteSrc,
      this.spriteAltSrc,
      GameElementTrex.State.Init,
      { min: 0, max: 150 });


    this.enemies = [];

    this._elapsed = 0;
    this._now = 0;
    this._dt = 0;   // time elapsed between frames
    this._step = 1 / 60;
    this._score = 0;
    this._level = 1;
    this._levelDuration = 30;
    this._levelElapsedTime = 0;
    this._minimumDistanceEnemy = this.boardSize.width * (2 / 3);
    this._totalEnemiesLevel = this._level * 2;
    this._enemiesGeneratedLevel = 0;
    this._enemyTimeSegment = this._levelDuration / (this._totalEnemiesLevel + 1);
    this._speedChangeLevelBase = 1.2;
    this._speedChangeLevel = this._speedChangeLevelBase;
    this.gameEnded = false;

    this.trex.startWalking();
    // clear keystates when starting.
    this.keyState = { jumpPressed: false, crouchPressed: false, escapePressed: false, extraPressed: false };

    this.isRunning = true;
    this._last = Date.now(); // timestamp
  }

  start(): void {
    this.init();
    requestAnimationFrame(this.frame/*.bind(this)*/);
  }

  stop(): void {
    this.gameEnded = true;
    this.isRunning = false;
  }


  reset(): void {
    this.keyState = { jumpPressed: false, crouchPressed: false, escapePressed: false, extraPressed: false };
    this.isRunning = false;
    this.gameEnded = false;
    this.imageLoaded = false;
    this._now = 0;
    this.boardSize = { height: 150, width: 400, scaling: 1 };
  }

  private frame() {
    this._now = Date.now();
    this._dt = this._dt + Math.min(1, (this._now - this._last) / 1000);

    while (this._dt > this._step && !this.gameEnded) {
      this._dt -= this._step;
      this._elapsed += this._step;
      this._score += this._step;
      this._levelElapsedTime += this._step;

      this.update(this._step);       // also renders because of databinging  ¯\_(ツ)_/¯
    }

    this._last = this._now;

    if (!this.gameEnded) {
      requestAnimationFrame(this.frame);
    } else {
      this._elapsed += this._dt;
      this._score += this._dt;
      this.trex.state = GameElementTrex.State.Dead;
    }
  }

  private update(elapsed: number) {
    const exit = this.keyState.escapePressed;
    let collided = false;


    if (!exit) {
      this.handleLevelChange();
      this.updateGameElements(elapsed); // also removes out of scope elements
      collided = this.collisionDetection();
    }

    if (exit || collided) {

      this.trex.state = GameElementTrex.State.Dead;
      this.isRunning = false;
      this.gameEnded = true;
      return;
    }

    this.maybeGenerateEnemy2();
  }

  private handleLevelChange(): void {
    // if level elapsed
    if (this._levelElapsedTime >= this._levelDuration) {
      this._levelElapsedTime -= this._levelDuration;
      ++this._level;
      this._totalEnemiesLevel = this._level * 2;
      this._enemiesGeneratedLevel = 0;

      this._enemyTimeSegment = this._levelDuration / this._totalEnemiesLevel;

      // Godmode resets every 3 levels.
      if (this.mod(this._level, 3) === 0 || this._level === 1) {
        this.trex.godUsed = false;
      }

      const oldSpeedChange = this._speedChangeLevel;

      this._speedChangeLevel = this._speedChangeLevelBase + (Math.log(this._level) * (3 / 4));

      this.trex.gameElement.speedMultiplicator = this._speedChangeLevel;

      this.enemies.forEach(el => {
        el.speedMultiplicator = this._speedChangeLevel;
      });

      this._minimumDistanceEnemy = this._minimumDistanceEnemy * (1 + (oldSpeedChange / this._speedChangeLevel));

    }
  }

  private updateGameElements(elapsed: number): void {
    // handle Trex
    let jump = false;
    let crouch = false;
    let extra = false;

    if (this.keyState.crouchPressed) {
      crouch = true;
    }

    if (this.keyState.jumpPressed) {
      jump = true;
    }

    if (this.keyState.extraPressed) {
      extra = true;
    }

    this.trex.update(crouch, jump, extra, elapsed, this._speedChangeLevel);

    // handle enemies
    this.enemies.forEach(element => {
      element.update(this._speedChangeLevel);
    });

    // remove enemies which are off canvas
    this.enemies = this.enemies.filter((el) => el.right >= 0);
  }

  // True if Trex collided with at least 1 enemy.
  private collisionDetection(): boolean {
    if (this.trex.isGod) {
      const enemiesLength = this.enemies.length;

      this.enemies = this.enemies.filter((el) => !this.overlap(el, this.trex.gameElement));
      this._score += (enemiesLength - this.enemies.length) * 10; // 10000 points plus
      return false;
    } else {
      return this.enemies.some((el) => this.overlap(el, this.trex.gameElement));
    }

  }

  private overlap(el1: GameElement, el2: GameElement): boolean {
    return !((el1.right < el2.left) ||
      (el1.left > el2.right) ||
      (el1.bottom > el2.top) ||
      (el1.top < el2.bottom));
  }

  // V2, better and sexy.
  private maybeGenerateEnemy2(): void {
    if (this._enemiesGeneratedLevel >= this._totalEnemiesLevel) {
      return;
    }

    let lastElement: GameElement;

    try {
      lastElement = this.enemies.reduce((acc, x) => {
        return acc > x ? acc : x;
      });
    } catch (error) { }

    // There is not enough space between enemies.
    if (lastElement && (this.boardSize.width - lastElement.right) < this._minimumDistanceEnemy) {
      return;
    }

    // this._totalEnemiesLevel         // n
    // this._enemiesGeneratedLevel     // k
    // this._enemyTimeSegment          // f

    // y = how much should have been generated inclusive this time segment
    const enemiesShould = Math.ceil(this._levelElapsedTime / this._enemyTimeSegment);

    // We have enough for now.
    if (this._enemiesGeneratedLevel >= enemiesShould) {
      return;
    }

    // Generate a probability.
    const p = (Math.random() * 0.5) + (1 - (((enemiesShould * this._enemyTimeSegment) - this._levelElapsedTime) / this._enemyTimeSegment));

    if (this.randomBoolean(p)) {
      this.enemies.push(this.randomEnemy());
      ++this._enemiesGeneratedLevel;
    }
  }

  // The old legacy generator, only as backup.
  private maybeGenerateEnemy(): void {
    // ENough enemies for this level.
    if (this._enemiesGeneratedLevel >= this._totalEnemiesLevel) {
      return;
    }

    let lastElement: GameElement;

    try {
      lastElement = this.enemies.reduce((acc, x) => {
        return acc > x ? acc : x;
      });
    } catch (error) {
    }

    // There is not enough space between enemies.
    if (lastElement && (this.boardSize.width - lastElement.right) < this._minimumDistanceEnemy) {
      return;
    }

    // How much time does the enemy has approximately do be generated.
    const enemyTimeFrame = this._levelElapsedTime - (this._enemiesGeneratedLevel * this._enemyTimeSegment);
    // How likely it is that an enemy will be generated this time.
    let pEnemy: number;

    // We are ahead of the schedule.
    if (enemyTimeFrame <= 0) {
      pEnemy = 0.2;
    } else if (enemyTimeFrame <= this._enemyTimeSegment) {
      pEnemy = 1 - ((this._enemyTimeSegment - enemyTimeFrame) / this._enemyTimeSegment);
    } else {
      pEnemy = 1;
    }

    if (this.randomBoolean(pEnemy)) {

      const enemy = this.randomEnemy();

      this.enemies.push(this.randomEnemy());
      ++this._enemiesGeneratedLevel;
    }
  }

  private mod(x: number, y: number): number {
    return ((x % y) + y) % y;
  }

  private randomBoolean(p: number): boolean {
    return Math.random() >= p;
  }

  private randomInteger(min: number, max: number): number {
    return Math.round(min + (Math.random() * (max - min)));
  }

  private randomEnemy(): GameElement {

    const keys = Object.keys(GameElement.EnemySubtypes);


    // Get random type of enemy.
    const enemyTypeIndex = this.randomInteger(0, keys.length - 1);
    const enemyType: GameElement.Type = keys[enemyTypeIndex] as GameElement.Type;

    if (enemyType === GameElement.Type.Bird) {
      // Bird has random y position, but no subtypes.
      const y = this.randomInteger(0, this.boardSize.height - GameElement.GameElementSize[GameElement.Type.Bird].height);

      return new GameElement(GameElement.Type.Bird, { xPos: this.boardSize.width, yPos: y }, this.spriteSrc, 0, this._speedChangeLevel);
    } else {
      // Its a small or large cactus.
      // Which subtype:
      const subtype = this.randomInteger(0, GameElement.EnemySubtypes[keys[enemyTypeIndex]].numberOfSubtypes - 1);

      return new GameElement(enemyType, { xPos: this.boardSize.width, yPos: 0 }, this.spriteSrc, subtype, this._speedChangeLevel);
    }
  }
}
