import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataConnectionService } from './data-connection.service';

@Injectable({
  providedIn: 'root'
})
export class DataUserService {
  private _tokenName: string;
  private _unameTokenName: string;
  private _userName: string;
  private _isLoggedIn: boolean;


  public get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }


  public set isLoggedIn(v: boolean) {
    if (this._isLoggedIn !== v) {
      this._isLoggedIn = v;
    }
  }


  get userToken(): string {
    let tok = localStorage.getItem(this._tokenName);

    if (tok == null || tok === undefined) {
      tok = '';
    }

    return tok;
  }

  set userToken(tok: string) {
    try {
      localStorage.setItem(this._tokenName, tok);
      this._isLoggedIn = true;
    } catch (error) { }
  }

  get userName(): string {
    return this._userName;
  }


  set userName(v: string) {
    this._userName = v;

    try {
      localStorage.setItem(this._unameTokenName, v);
    } catch (error) { }
  }


  constructor(private httpClient: HttpClient, private connectionService: DataConnectionService) {
    this._isLoggedIn = false;
    this._tokenName = 'userToken';
    this._unameTokenName = 'userName';

    const uname = localStorage.getItem(this._unameTokenName);

    if (uname !== null && uname !== undefined) {
      this.userName = uname;
    }
  }

  deleteUserData(): void {
    try {
      localStorage.removeItem(this._tokenName);
      this._isLoggedIn = false;
      localStorage.removeItem(this._unameTokenName);
    } catch (error) { }
  }

  checkTokenValidity() {
    return this.httpClient.get(this.connectionService.getCheckValidityPath);
  }
}
