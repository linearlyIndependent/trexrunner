import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { DataConnectionService } from '../_services/data-connection.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpClient: HttpClient,
    private connectionService: DataConnectionService) {

  }


  loginUser(uname: string, passwd: string) {
    return this.httpClient.post(
      this.connectionService.postLoginPath,
      'username=' + uname + '&password=' + passwd,
      { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) });
  }

  registerUser(uname: string, passwd: string) {
    return this.httpClient.post(
      this.connectionService.postRegisterPath,
      'username=' + uname + '&password=' + passwd,
      { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) });
  }

  logoutUser() {
    return this.httpClient.delete(this.connectionService.postLoginPath);
  }

}
