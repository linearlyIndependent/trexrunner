import { Component, OnInit } from '@angular/core';

import { StatsUserService } from '../_services/stats-user.service';
import { DataUserService } from '../_services/data-user.service';
import { ScoreEntry } from '../_container-classes/score-entry';

@Component({
  selector: 'app-stats-user',
  templateUrl: './stats-user.component.html',
  styleUrls: ['./stats-user.component.scss']
})
export class StatsUserComponent implements OnInit {
  private _scores: ScoreEntry[];
  private _validStats: boolean;

  public get scores(): ScoreEntry[] {
    return this._scores;
  }

  public get validStats(): boolean {
    return this._validStats;
  }

  public get inValidStats(): boolean {
    return !this._validStats;
  }

  constructor(private userStatService: StatsUserService, private userDataService: DataUserService) { }

  ngOnInit() {
    this._validStats = false;
    this._scores = [];
    this.getScores();
  }

  private getScores() {
    this.userStatService.getScores()
      .subscribe(data => {
        if (data && data['scores']) {
          this._scores = (data['scores']).map(x => new ScoreEntry(x['comment'], x['score'], x['date']));
          this._validStats = this._scores.length > 0;
        } else {
        }
      }, err => {
      });
  }
}
