import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from '../_services/login.service';
import { DataUserService } from '../_services/data-user.service';
import { RoutingHistoryService } from '../_services/routing-history.service';
import { GameService } from '../_services/game.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  public get name(): string {
    return this.userDataService.userName;
  }


  constructor(
    private loginService: LoginService,
    private userDataService: DataUserService,
    private gameService: GameService,
    private router: Router,
    private routerHistory: RoutingHistoryService) { }

  ngOnInit() {
  }

  yesClickedCallback(): void {
    this.gameService.stop();

    this.loginService.logoutUser()
      .subscribe(data => {
        this.userDataService.userName = '';
        this.userDataService.deleteUserData();
        this.routerHistory.previous = '';
        this.router.navigate(['/login']);
      }, err => {
      });
  }

  noClickedCallback(): void {

    if (this.routerHistory.previous !== '') {
      this.router.navigate([this.routerHistory.previous]);
    } else {
      this.router.navigate(['/game']);
    }
  }
}
